# FINAL PROJECT

Here is my final project for JSCRIPT 200 A: Programming For The Browser. It includes the following features to meet the assignment requirements:

1. One or more timing functions
2. One or more fetch requests to a 3rd party API
3. Sets, updates or changes local storage
4. Contains form fields, validates those fields

You can also find the deployed site [here](https://wandrew8.github.io/News-reader/).
